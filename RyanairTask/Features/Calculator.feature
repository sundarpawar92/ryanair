﻿Feature: Ryaniar Calculator in Different Modes

@Story 1 
Scenario Outline: Programmer mode calculator, enter a value and verify HEX, DEC, OCT, and BIN
	Given I am opening calculator in Programmer mode
	And First Number should be entered as 8
	And Select Bitwise and select operator
	And Click on  OR operator
	And Second Number should be Entered as 5
	And I Will Click equal for result
	And I Will Verify Hex Value will be D
	And I Will Verify Dec Value will be 13
	And I Will Verify OCT Value should be 15
	And I Will Verify BIN Value should be 1101
	Then I Will Verify the result should be 13
	And I Will Close the Calculator

@Story 2	
Scenario Outline: Scientific mode Calculator, finding the log value and closing calculator
     Given I am opening calculator in  Scientific Mode
	 And First Number should be entered 5 
	 And Click on Log function
	 And Second Number should be Entered 7
	 When Will Click equal to get result
	 Then I Will Verify the The result should be 7
	 And Will Close the Calculator.

@Story 3	
Scenario Outline: Date Calculation Mode, enter dates and verify day and week results
	Given I am opening calculator in Date Calculator Mode
	And Select date in from date
	And Go back to previous date 
	And Select date from month
	And Select date in to date
	And Select previous date
	And Select date of that month
	Then The result will be 1 week, 1 day
	And Will Close Close the Calculator


@Story 4
Scenario Outline: Addition In Standard Mode
	Given I am opening calculator in Standard Mode
	And Enter the First Number 5
	And Click  Plus
	And Enter the Second Number 7
	And Click on Equals
	Then The result must be 12
	And Will Close Close the Calculator

 Scenario Outline: Divide In Standard Mode
	Given I am opening  calculator in Standard Mode
	And Enter First Number 7
	And Click Divide By
	And Enter Second Number 7
	And Click Equals
	Then The result should be 1
	And I will close calculator
	
Scenario Outline: Check History In standard mode
Given Open calc in Standard Mode
And Click on History
And Check Result
And Close Calculator
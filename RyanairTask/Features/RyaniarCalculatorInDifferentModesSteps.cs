﻿using OpenQA.Selenium.Appium;
using OpenQA.Selenium.Appium.Windows;
using System;
//using System.Threading;
using TechTalk.SpecFlow;

namespace RyanairTask.Features
{
    [Binding]
    public class RyaniarCalculatorInDifferentModesSteps
    {
        private WindowsDriver<WindowsElement> _driver;

        public object CalculatorResult { get; private set; }

        [Given(@"I am opening calculator in Programmer mode")]
        public void GivenIAmOpeningCalculatorInProgrammerMode()
        {
            var options = new AppiumOptions();

            options.AddAdditionalCapability("app", "Microsoft.WindowsCalculator_8wekyb3d8bbwe!App");
            options.AddAdditionalCapability("deviceName", "WindowsPC");
            _driver = new WindowsDriver<WindowsElement>(new Uri("http://127.0.0.1:4723"), options);
            _driver.FindElementByName("Open Navigation").Click();
            _driver.FindElementByName("Programmer Calculator").Click();
            _driver.FindElementByName("Clear").Click();
            //_driver.Manage().Timeouts().ImplicitWait = TimeSpan.FromSeconds(5);
        }
        
        [Given(@"First Number should be entered as (.*)")]
        public void GivenFirstNumberShouldBeEnteredAs(int p0)
        {
            //Thread.Sleep(15000);
            _driver.FindElementByName("Eight").Click();
        }
        
        [Given(@"Select Bitwise and select operator")]
        public void GivenSelectBitwiseAndSelectOperator()
        {
            _driver.FindElementByName("Bitwise").Click();
        }
        
        [Given(@"Click on  OR operator")]
        public void GivenClickOnOROperator()
        {
            _driver.FindElementByName("Or").Click();
        }
        
        [Given(@"Second Number should be Entered as (.*)")]
        public void GivenSecondNumberShouldBeEnteredAs(int p0)
        {
            _driver.FindElementByName("Five").Click();
        }
        
        [Given(@"I Will Click equal for result")]
        public void GivenIWillClickEqualForResult()
        {
            _driver.FindElementByName("Equals").Click();
        }
        
        [Given(@"I Will Verify Hex Value will be D")]
        public void GivenIWillVerifyHexValueWillBeD()
        {
            _driver.FindElementByName("HexaDecimal D ").Click();
        }
        
        [Given(@"I Will Verify Dec Value will be (.*)")]
        public void GivenIWillVerifyDecValueWillBe(int p0)
        {
            _driver.FindElementByName("Decimal 13").Click();
        }
        
        [Given(@"I Will Verify OCT Value should be (.*)")]
        public void GivenIWillVerifyOCTValueShouldBe(int p0)
        {
            _driver.FindElementByName("Octal 1 5 ").Click();
        }
        
        [Given(@"I Will Verify BIN Value should be (.*)")]
        public void GivenIWillVerifyBINValueShouldBe(int p0)
        {
            _driver.FindElementByName("Binary 1 1 0 1 ").Click();
        }
        
        [Then(@"I Will Verify the result should be (.*)")]
        public object ThenIWillVerifyTheResultShouldBe(int p0)
        {
            return CalculatorResult;
        }
        
        [Then(@"I Will Close the Calculator")]
        public void ThenIWillCloseTheCalculator()
        {
            _driver.FindElementByName("Close Calculator").Click();
        }

        [Given(@"I am opening calculator in  Scientific Mode")]
        public void GivenIAmOpeningCalculatorInScientificMode()
        {
            var options = new AppiumOptions();
            options.AddAdditionalCapability("app", "Microsoft.WindowsCalculator_8wekyb3d8bbwe!App");
            options.AddAdditionalCapability("deviceName", "WindowsPC");
            _driver = new WindowsDriver<WindowsElement>(new Uri("http://127.0.0.1:4723"), options);
            _driver.FindElementByName("Open Navigation").Click();
            _driver.FindElementByName("Scientific Calculator").Click();
            _driver.FindElementByName("Clear").Click();
            //_driver.Manage().Timeouts().ImplicitWait = TimeSpan.FromSeconds(5);
        }

        [Given(@"First Number should be entered (.*)")]
        public void GivenFirstNumberShouldBeEntered(int p0)
        {
            _driver.FindElementByName("Five").Click();
        }

        [Given(@"Click on Log function")]
        public void GivenClickOnLogFunction()
        {
            _driver.FindElementByName("Log").Click();
        }
        [Given(@"Second Number should be Entered (.*)")]
        public void GivenSecondNumberShouldBeEntered(int p0)
        {
            _driver.FindElementByName("Seven").Click();
        }
        [When(@"Will Click equal to get result")]
        public void WhenWillClickEqualToGetResult()
        {
            _driver.FindElementByName("Equals").Click();
        }
        [Then(@"I Will Verify the The result should be (.*)")]
        public object ThenIWillVerifyTheTheResultShouldBe(int p0)
        {
            return CalculatorResult;
        }
        [Then(@"Will Close the Calculator\.")]
        public void ThenWillCloseTheCalculator_()
        {
            _driver.FindElementByName("Close Calculator").Click();
        }



        [Given(@"I am opening calculator in Date Calculator Mode")]
        public void GivenIAmOpeningCalculatorInDateCalculatorMode()
        {
            var options = new AppiumOptions();
            options.AddAdditionalCapability("app", "Microsoft.WindowsCalculator_8wekyb3d8bbwe!App");
            options.AddAdditionalCapability("deviceName", "WindowsPC");
            _driver = new WindowsDriver<WindowsElement>(new Uri("http://127.0.0.1:4723"), options);
            _driver.FindElementByName("Open Navigation").Click();
            _driver.FindElementByName("Date Calculation Calculator").Click();
            //_driver.FindElementByName("Clear").Click();
        }
        [Given(@"Select date in from date")]
        public void GivenSelectDateInFromDate()
        {
            _driver.FindElementByName("From").Click();
        }

        [Given(@"Go back to previous date")]
        public void GivenGoBackToPreviousDate()
        {
            _driver.FindElementByName("Previous").Click();
        }

        [Given(@"Select date from month")]
        public void GivenSelectDateFromMonth()
        {
            _driver.FindElementByName("4").Click();
        }

        [Given(@"Select date in to date")]
        public void GivenSelectDateInToDate()
        {
            _driver.FindElementByName("To").Click();
        }
        [Given(@"Select previous date")]
        public void GivenSelectPreviousDate()
        {
            _driver.FindElementByName("Previous").Click();
        }

        [Given(@"Select date of that month")]
        public void GivenSelectDateOfThatMonth()
        {
            _driver.FindElementByName("10").Click();
        }
        [Then(@"The result will be (.*) week, (.*) day")]
        public Object ThenTheResultWillBeWeekDay(int p0, int p1)
        {
            return CalculatorResult;
        }
        [Then(@"Will Close Close the Calculator")]
        public void ThenWillCloseCloseTheCalculator()
        {
            _driver.FindElementByName("Close Calculator").Click();
        }

        [Given(@"I am opening calculator in Standard Mode")]
        public void GivenIAmOpeningCalculatorInStandardMode()
        {
            var options = new AppiumOptions();
            options.AddAdditionalCapability("app", "Microsoft.WindowsCalculator_8wekyb3d8bbwe!App");
            options.AddAdditionalCapability("deviceName", "WindowsPC");
            _driver = new WindowsDriver<WindowsElement>(new Uri("http://127.0.0.1:4723"), options);
            _driver.FindElementByName("Open Navigation").Click();
            _driver.FindElementByName("Standard Calculator").Click();
            _driver.FindElementByName("Clear").Click();
            _driver.Manage().Timeouts().ImplicitWait = TimeSpan.FromSeconds(5);

        }
        [Given(@"Enter the First Number (.*)")]
        public void GivenEnterTheFirstNumber(int p0)
        {
            _driver.FindElementByName("Five").Click();
        }
        [Given(@"Click  Plus")]
        public void GivenClickPlus()
        {
            _driver.FindElementByName("Plus").Click();
        }

        
        [Given(@"Enter the Second Number (.*)")]
        public void GivenEnterTheSecondNumber(int p0)
        {
            _driver.FindElementByName("Seven").Click();
        }

        [Given(@"Click on Equals")]
        public void ThenClickOnEquals()
        {
            _driver.FindElementByName("Equals").Click();
        }


        [Then(@"The result must be (.*)")]
        public object ThenTheResultMustBe(int p0)
        {
            return CalculatorResult;
        }
        [Then(@"I Will Close Close the Calculator")]
        public void TheniWillCloseCloseTheCalculator()
        {
            _driver.FindElementByName("Close Calculator").Click();
        }

        [Given(@"I am opening  calculator in Standard Mode")]
        public void GiveniAmOpeningCalculatorInStandardMode()
        {
            var options = new AppiumOptions();
            options.AddAdditionalCapability("app", "Microsoft.WindowsCalculator_8wekyb3d8bbwe!App");
            options.AddAdditionalCapability("deviceName", "WindowsPC");
            _driver = new WindowsDriver<WindowsElement>(new Uri("http://127.0.0.1:4723"), options);
            _driver.FindElementByName("Open Navigation").Click();
            _driver.FindElementByName("Standard Calculator").Click();
            _driver.FindElementByName("Clear").Click();
            _driver.Manage().Timeouts().ImplicitWait = TimeSpan.FromSeconds(5);

        }
        [Given(@"Enter First Number (.*)")]
        public void GivenEnterFirstNumber(int p0)
        {
            _driver.FindElementByName("Seven").Click();
        }

        [Given(@"Click Divide By")]
        public void GivenClickDivideBy()
        {
            _driver.FindElementByName("Divide by").Click();
        }
        [Given(@"Enter Second Number (.*)")]
        public void GivenenterSecondNumber(int p0)
        {
            _driver.FindElementByName("Seven").Click();
        }
        [Given(@"Click Equals")]
        public void GivenClickEquals()
        {
            _driver.FindElementByName("Equals").Click();
        }

        [Then(@"The result should be (.*)")]
        public object ThenTheResultShouldBe(int p0)
        {
            return CalculatorResult;
        }

        [Then(@"I will close calculator")]
        public void ThenIWillCloseCalculator()
        {
            _driver.FindElementByName("Close Calculator").Click();
        }



        [Given(@"Open calc in Standard Mode")]
        public void GivenOpenCalcInStandardMode()
        {
            var options = new AppiumOptions();
            options.AddAdditionalCapability("app", "Microsoft.WindowsCalculator_8wekyb3d8bbwe!App");
            options.AddAdditionalCapability("deviceName", "WindowsPC");
            _driver = new WindowsDriver<WindowsElement>(new Uri("http://127.0.0.1:4723"), options);
            _driver.FindElementByName("Open Navigation").Click();
            _driver.FindElementByName("Standard Calculator").Click();
            _driver.FindElementByName("Clear").Click();
            _driver.Manage().Timeouts().ImplicitWait = TimeSpan.FromSeconds(5);

        }

        [Given(@"Click on History")]
        public void GivenClickOnHistory()
        {
            _driver.FindElementByName("Open history flyout").Click();
        }

        [Given(@"Check Result")]
        public object GivenCheckResult()
        {
            return CalculatorResult;
        }

        [Given(@"Close Calculator")]
        public void GivenCloseCalculator()
        {
            _driver.FindElementByName("Close Calculator").Click();
        }

    }
}
